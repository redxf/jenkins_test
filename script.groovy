def buildApp() {
    echo "building the application..."
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "echo $PASS | docker login -u $USER --password-stdin"
    }
} 

def testApp() {
    echo 'testing the application...'
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
